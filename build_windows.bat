:: Copyright (C) 2023 The Qt Company Ltd.
::
:: SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
::

@echo off

mkdir build
mkdir deploy

copy dist\qtlicd.ini deploy\

cd build
cmake .. -G "Visual Studio 17 2022"
cmake --build .

copy bin\Debug\qtlicd.exe ..\deploy\
copy bin\Debug\licheck.exe ..\deploy\
copy bin\Debug\mocwrapper.exe ..\deploy\
copy bin\Debug\qtlicensetool.exe ..\deploy\

cd ..
