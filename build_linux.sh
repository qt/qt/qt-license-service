#!/usr/bin/env bash
# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
#


name=qtlicd
deploy_dir=deploy
INSTALL=false

function exit_fail {
    echo "ERROR! $1: Aborting installation"
    exit 1
}

function helptext {
    echo "Supported CLI arguments:"
    echo " --install  :  Install and run daemon after building"
    echo " --help     :  This help"
}

# Read command line options
function parse_cli_arguments {
    ret=0
    for arg in "$@"
    do
        case ${arg} in
        --install)
            INSTALL=true
          ;;
        --help)
            helptext
            exit 0
            ;;
        *)
            echo "Invalid CLI argument ${arg} given"
            helptext
            exit 1
            ;;
        esac
    done

    return ${ret}
}

parse_cli_arguments "$@"

# Create build dir (if not there)
mkdir -p build
if [ $? != 0 ]; then
    exit_fail "Failed to create build dir"
fi
cd build

# build
cmake ..
if [ $? != 0 ]; then
    exit_fail "Cmake failed"
fi

make clean
make
if [ $? != 0 ]; then
    exit_fail "Build failed"
fi

chmod +x bin/${name} bin/licheck bin/qtlicensetool bin/mocwrapper
cd ..
# Copy files for deployment dir (first create it)
mkdir -p ${deploy_dir}
if [ $? != 0 ]; then
    exit_fail "Failed to create deploy dir"
fi

cp build/bin/${name} ${deploy_dir}/
cp build/bin/licheck ${deploy_dir}/
cp build/bin/qtlicensetool ${deploy_dir}/
cp build/bin/mocwrapper ${deploy_dir}/
cp dist/${name}.ini ${deploy_dir}/
cp dist/linux_service_scripts/${name}.service ${deploy_dir}/
cp dist/linux_service_scripts/installer.sh ${deploy_dir}/
cp dist/linux_service_scripts/uninstaller.sh ${deploy_dir}/

#
# Install & start the daemon
#
if [ "${INSTALL}" == true ]; then
    cd ${deploy_dir}
    echo "Installing.... Need to be root"
    sudo sh installer.sh
fi

echo "Done."
