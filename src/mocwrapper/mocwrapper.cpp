/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "mocwrapper.h"

#include "commonsetup.h"

#include "licdsetup.h"
#include "tcpclient.h"
#include "version.h"
#include "utils.h"

using namespace QLicenseService;

int main(int argc, char *argv[])
{
    // First check if version is asked
    if (argc > 1) {
        if (std::string(argv[1]) == "--version") {
            std::cout << "MOC-wrapper v" << MOCWRAPPER_VERSION << " "
                    << COPYRIGHT_TEXT << std::endl;
            return 0;
        }
    }

    if (requestLicense(argv[0]) != 0) {
        std::cout << "WARNING! No valid license to run " << MOCWRAPPER_APP_NAME;
        if (BLOCK_IF_NO_LICENSE) {
            std::cout << ": Aborting\n";
            exit(EXIT_FAILURE);
        }
        std::cout << std::endl;
    }
    // Continue by running MOC...
    if (forward_call(argc, argv) != 0) {
        std::cout << "Failed to start " << MOCWRAPPER_APP_NAME << std::endl;
    }
}

int forward_call(int argc, char *argv[])
{
    std::string origCmd = utils::strToLower(MOCWRAPPER_APP_NAME);
    std::string executable = ORIGINAL_MOC_PREFIX + origCmd;
    std::string cmd = std::string(argv[0]) + '\0';
    cmd.replace(cmd.find(origCmd), origCmd.length(), executable);
    std::string args = "";
    for (int i = 1; i < argc; i++) {
        args += " ";
        args += argv[i];
        args += '\0';
    }
    cmd += args;
#ifdef _WIN32
    int retval = (int)_execve(cmd.c_str(), argv, NULL);
#else
    int retval = execve(cmd.c_str(), argv, NULL);
#endif
    if (retval != 0)
        fprintf(stderr, "Calling '%s' failed, errno = %d\nCommand: \n%s\n",
                        MOCWRAPPER_APP_NAME, errno, cmd.c_str());
    return retval;
}

int requestLicense(const std::string &mocBinPath)
{
    LicdSetup user(e_set_type_moc, LicdSetup::getQtAppDataLocation() + USER_SETTINGS_FILE, mocBinPath);
    if (user.initSettings() == e_file_not_present) {
        return -1;
    }
    // Read user info
    std::string addr = user.get("licd_addr");
    uint16_t port = utils::strToInt(user.get("licd_port"));
    std::stringstream ss;
    ss << "license -u " << user.get("user_id") <<  " -e " << user.get("user_email") << " -i " << user.get("license_id")
            << " -a " << MOCWRAPPER_APP_NAME << " -v " << MOCWRAPPER_VERSION;
    if (user.get("license_server_addr").length() > 0) {
        ss << " -l " << user.get("license_server_addr") << ":" << user.get("license_server_port");
    }
    TcpClient tcp(addr, port);
    std::string reply;
    int result = tcp.sendAndReceive(ss.str(), reply);
    if (result != e_tcp_success) {
        std::cout << "Failure: " << tcp.errorString(result) << std::endl;
        return -1; // TCP/IP error
    }
    reply = utils::trimStr(reply);
    if (reply != "License acquired.")
        std::cout << reply << std::endl;
    if (reply.substr(0,2) == "No" || reply.substr(0,5) == "ERROR") {
        return -1; // No license
    }
    return 0;
}
