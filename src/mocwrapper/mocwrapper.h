/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <cstring>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <map>
#include <cstdlib>
#ifdef _WIN32
    #define WIN32_LEAN_AND_MEAN
    #include <process.h>
#endif

#define BUFFER_SIZE 1024

int getUserInfo(const std::string &filepath);
int requestLicense(const std::string &mocBinPath);
int forward_call(int argc, char *argv[]);
std::string parseSettingTag(const std::string &mocBinPath);
int createUserSettingsFile();
int updateUserSettingsFile(const std::string &settingsTag);
static std::string getUserFileSkeleton();
static std::string getTaggedSettingsSkeleton(const std::string &tag);
