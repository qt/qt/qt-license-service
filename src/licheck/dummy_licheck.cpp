/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

// Dummy replacement for legacy licheck to have
// Qt5 qmake satisfied with return value of '0'
int main(int argc, char *argv[]) {
    return 0;
}
