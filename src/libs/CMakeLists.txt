# Copyright (C) 2023 The Qt Company Ltd.
#
# Published under GPL-3.0 with Qt-GPL-exception-1.0
#

add_subdirectory(3rdparty)
add_subdirectory(qlicenseservice)
