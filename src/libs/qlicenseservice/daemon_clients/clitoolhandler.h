/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include "clienthandler.h"
#include "jsonhandler.h"
#include "utils.h"

namespace QLicenseService {

class CliToolHandler : public virtual ClientHandler {

    public:
        CliToolHandler(const RequestInfo &request, const LicdSetup &settings)
                : ClientHandler(request, settings) {
            std::cout << "Client: CLI-tool\n";
        }

        bool isLicenseRequestDue() override { return true; }
        bool isCachedReservationValid(std::string &reply) override { return false; }

    private:

        void buildRequestJson() override
        {
            if (m_request.reqType == RequestType::server_version) {
                m_request.payload = "";
                return;
            }
            // Permanent operation:
            std::stringstream pay;
            pay << "{";
            pay<< "\"license_number\":" << "\"" << m_request.licenseId << "\",";
            pay << "\"user_id\":" << "\"" << m_request.userId << "\",";
            pay << "\"qt5_license_key\":" << "\"" << m_settings.get("license_key") << "\",";
            pay << "\"operation\":" << "\"" << m_request.operation << "\"";
            pay << "}";
            m_request.payload = pay.str();
        }

        int parseAndSaveResponse(std::string &response) override
        {
            JsonHandler json(response);
            std::stringstream ss;

            if (m_request.reqType == RequestType::server_version) {
                response = "Qt License Server v";
                response += json.get("version");
                return 0;
            }

            if (json.get("status") != "true" ) {
                if (response == "License fully reserved") {
                    response = replyString[e_license_pool_full];
                } else if (response.find("cannot be early released") != std::string::npos) {
                    response = replyString[e_no_permanent_to_release];
                } else {
                    response = replyString[e_license_rejected];
                }
                // Do not touch the license file, just return
                return 1;
            } else if (m_request.reqType == RequestType::long_term_request) {
                // long-term request: Have "message" field from response JSON directly as a reply to the user
                response = json.get("message");

                // Add timestamp into license JSON
                json.add("last_timestamp", utils::getTimestampNow());

                int result = utils::writeToFile(m_request.licenseFile, json.dump(4));
                if (result != 0) {
                    std::cout << "ERROR saving license file: '" << m_request.licenseFile << "': " << strerror(result) << std::endl;
                }
            }
            return 0;
        }
};

} // namespace QLicenseService
