/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

#include "request.h"
#include "licdsetup.h"
#include "license.h"

namespace QLicenseService {

class ClientHandler
{
    public:
        ClientHandler(const RequestInfo &request, const LicdSetup &settings) :
            m_request(request),
            m_settings(settings)
        { }
        virtual ~ClientHandler()
        { }

        std::vector<std::string> params;

        virtual bool isCachedReservationValid(std::string &reply) = 0;
        virtual bool isLicenseRequestDue() = 0;
        virtual int parseAndSaveResponse(std::string &response) = 0;
        virtual void buildRequestJson() = 0;
        virtual void prepareRelease() { return; }
        virtual void release() { return; }

        void updateLicense(const std::string &responseJson);
        int parseRequest();
        RequestInfo getRequest() { return m_request; }
        int getSocketId() { return m_request.socketId; }
        RequestType getRequestType() { return m_request.reqType; }
        std::string getParentReservationId() { return m_request.parentReservationId; }
        std::string getReservationId() { return m_license.reservation_id; }
        uint16_t getNumberOfClients() { return m_childClients.size(); }
        void resetTime();
        bool hasFloatingLicense() { return m_floatingLicense; }
        bool hasParent() { return m_hasParent; }
        void addChildClient(ClientHandler *client);
        bool removeChildClient(uint16_t socketId);
        bool hasChildren() { return !m_childClients.empty(); }
        bool stopped() { return m_stoppedWorking; }
        void removeCachedFile();
        bool clientInStorage(uint16_t socketId);

    protected:
        std::unordered_map<uint16_t, ClientHandler*> m_childClients;
        License      m_license;
        RequestInfo  m_request;
        LicdSetup    m_settings;;
        uint64_t     m_updateInterval;
        bool m_floatingLicense = false;
        bool m_hasParent = false;
        bool m_stoppedWorking = false;

        bool checkLicenseExpiryTime(std::string &reply);
        bool checkLeewayTime(std::string &reply);
};

static std::map<RequestReply, std::string> replyString {
    {e_bad_request, "ERROR Bad request"},
    {e_license_granted, "License acquired."},
    {e_license_rejected, "No valid license acquired"},
    {e_no_conn_leeway, "License granted with warning: No server connection. Leeway time left: "},
    {e_license_pool_full, "All licenses in use. No more license available on the server."},
    {e_no_permanent_to_release, "No permanent reservations available to be released"},
    {e_bad_connection, "No connection to server. Try again later."}
};

} // namespace QLicenseService
