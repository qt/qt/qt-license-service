/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include "clienthandler.h"
#include "jsonhandler.h"
#include "utils.h"

namespace QLicenseService {

class SquishHandler : public ClientHandler {

    public:
        SquishHandler(const RequestInfo &request, const LicdSetup &settings)
                : ClientHandler(request, settings)
        {
            m_updateInterval = utils::strToInt(m_settings.get("squish_report_interval"));
            m_request.operation = OP_ADD_RESERVATION;
            m_request.startTimestamp = utils::getTimestampNow();
            m_license.last_timestamp = 0;
            m_floatingLicense = true;
            std::cout << "Client: Squish\n";
        }

        bool isCachedReservationValid(std::string &reply) override
        {
            // Floating license requires constant connection - no cache check
            return false;
        }

        void buildRequestJson() override
        {
            std::stringstream pay;
            pay << "{";
            pay << "\"license_number\":\"" << m_request.licenseId << "\",";
            pay << "\"user_id\":\"" << m_request.userId << "\",";
            pay << "\"hw_id\":\"" << m_settings.get("hw_id") << "\",";
            pay << "\"qt5_license_key\":" << "\"" << m_settings.get("license_key") << "\",";
            pay << "\"operation\":\"" << m_request.operation << "\",";
            pay << "\"type\":\"" << m_request.runnerType << "\",";
            pay << "\"src\":\"" << m_request.appName << "\",";
            pay << "\"host_os\":\"" << m_settings.get("host_os") << "\",";
            pay << "\"src_version\":\"" << m_request.appVersion << "\",";
            if (m_hasParent) {
                pay << "\"reservation_id\":\"" << m_request.parentReservationId << "\",";
            }
            if (m_request.operation == OP_QA_RELEASE_RESERVATION) {
                pay << "\"custom_duration\":\"" << m_request.stopTimestamp
                                    - m_request.startTimestamp << "\",";
            }
            pay << "\"email\":\"" << m_request.email << "\"";
            pay << "}";
            m_request.payload = pay.str();
        }

        int parseAndSaveResponse(std::string &response) override
        {
            JsonHandler json(response);
            std::stringstream ss;
            m_license.status = (json.get("status") == "true") ? true : false;
            if (m_license.status) {
                ss << replyString[e_license_granted];
                ss << " expiry_date=" << json.get("expiry_date");
                ss << " license_id=" << json.get("license_number");
                ss << " reservation_id=" << json.get("reservation_id");
                response = ss.str();
            } else {
                if (json.get("message") == "License fully reserved") {
                    response = replyString[e_license_pool_full];
                } else {
                    response = replyString[e_license_rejected];
                }
                // Do not save the response, just return
                return 1;
            }
            m_license.message = json.get("message");
            m_license.license_id = json.get("license_number");
            m_license.reservation_id = json.get("reservation_id");
            m_license.license_key = json.get("license_key");
            // Update timestamp into license
            m_license.last_timestamp = utils::getTimestampNow();

            // Save the JSON, adding timestamp
            json.add("last_timestamp", m_license.last_timestamp);
            int result = utils::writeToFile(m_request.licenseFile, json.dump(4));
            if (result != 0) {
                std::cout << "ERROR saving license file: '" << m_request.licenseFile << "': " << strerror(result) << std::endl;
            }
            return 0;
        }

        void prepareRelease() override
        {
            m_stoppedWorking = true;
            std::cout << (m_hasParent?"Child ":"Parent") << " process stopped: prepare for release\n";
        }

        void release() override
        {
            m_request.operation = OP_QA_RELEASE_RESERVATION;
            m_request.stopTimestamp = utils::getTimestampNow();
            std::cout << "Releasing " << (m_hasParent?"child ":"parent") << " process\n";
        }

        bool isLicenseRequestDue() override
        {
            // First check if this client is a subprocess - no reporting
            if (m_hasParent) {
                return false;
            }
            uint64_t now = utils::getTimestampNow();
            if (m_license.last_timestamp == 0)
                return true; // startup
            if ((now - m_license.last_timestamp) > m_updateInterval)
                return true;
            return false;
        }
};

} // namespace QLicenseService
