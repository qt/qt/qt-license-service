/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <iostream>
#include <string>
#include <map>
#define BUFFER_SIZE 1024

#if __APPLE__ || __MACH__ || __linux__
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
    #include <netdb.h>
    typedef int type_socket;
#else
    #define WIN32_LEAN_AND_MEAN
    #include <windows.h>
    #include <winsock2.h>
    #include <ws2tcpip.h>
    #include <process.h>
    // Need to link with Ws2_32.lib
    #pragma comment (lib, "Ws2_32.lib")
    typedef size_t ssize_t;
    typedef SOCKET type_socket;
#endif

namespace QLicenseService {

enum TcpReturnValues {
    e_tcp_success           = 0,
    e_tcp_error_conn        = 1,
    e_tcp_error_send        = 2,
    e_tcp_error_recv        = 3,
    e_tcp_error_hostname    = 4,
    e_tcp_fail_socket       = 5
};

class TcpClient
{
public:
    TcpClient(const std::string &connAddr, uint16_t port);
    ~TcpClient() { doCloseSocket(); }

    int sendAndReceive(const std::string &message, std::string &reply);
    std::string errorString(int errCode) {return m_tcpReturnStr[errCode];};

private:
    type_socket m_socketFD;
    sockaddr_in m_server;
    std::map<int, std::string> m_tcpReturnStr = {
        {e_tcp_success,         "TCP: Ok"},
        {e_tcp_error_conn,      "TCP: Error when connecting"},
        {e_tcp_error_hostname,  "TCP: Error finding the hostname"},
        {e_tcp_error_recv,      "TCP: Error receiving data"},
        {e_tcp_error_send,      "TCP: Error sending data"},
        {e_tcp_fail_socket,     "TCP: Fail setting up the socket"}
    };

    void doCloseSocket()
    {
#if __APPLE__ || __MACH__  || __linux__
        close(m_socketFD);
#else
        closesocket(m_socketFD);
#endif
    }

};

} // namespace QLicenseService
