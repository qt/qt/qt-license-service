/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <ctime>
#include <cassert>
#if _WIN32
    // Windows
    #define WIN32_LEAN_AND_MEAN
    //#include <windows.h>
#else
#endif
#include "httpclient.h"
#include "tcpserver.h"
#include "licdsetup.h"
#include "clienthandler.h"

namespace QLicenseService {

class Licenser
{
public:
    explicit Licenser(uint16_t tcpPort = 0, const std::string &settingsPath = "");
    ~Licenser();

    int listen();
    int sendServerRequest(const RequestInfo &request, std::string &reply);

private:
    HttpClient *m_http;
    TcpServer *m_tcpServer;
    uint64_t m_mocInterval;
    std::string m_infoString;
    std::unordered_map<uint16_t, ClientHandler*> m_floatingClients;

    ClientHandler *parseInputAndCreateCLient(uint16_t socketId, const std::string &incoming);
    bool addClientToParent(ClientHandler *const client);
    std::string checkReservations();
    std::string getDaemonVersion();
    int checkTasksDue();
    void removeReservation(int socketId);
    bool clientInStorage(uint16_t socketId);

    LicdSetup *m_settings;
};

} // namespace QLicenseService
