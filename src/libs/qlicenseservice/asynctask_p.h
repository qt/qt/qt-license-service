/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <future>

namespace QLicenseService {

template <typename T>
class AsyncTaskPrivate
{
public:
    T result()
    {
        if (!m_future.valid())
            return T();

        return m_future.get();
    }

public:
    std::future<T> m_future;
};

template <>
class AsyncTaskPrivate<void>
{
public:
    void result()
    {
        return;
    }

public:
    std::future<void> m_future;
};

} // namespace QLicenseService
