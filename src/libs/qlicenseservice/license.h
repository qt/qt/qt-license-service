/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <string>

namespace QLicenseService {

struct License {
    uint64_t last_timestamp = 0;    //    |
    uint64_t current_timestamp = 0; //    | For internal use only, not in server resp JSON
    uint64_t expiry_epoch = 0;      //    |
    bool status = false;
    std::string message;
    std::string user_id;
    std::string license_key;
    std::string license_id;
    std::string expiry_date;
    std::string reservation_id;
    uint16_t leeway_hours = 0;
};

} // namespace QLicenseService
