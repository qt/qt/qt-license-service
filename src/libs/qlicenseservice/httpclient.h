/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include <iostream>
#include <sstream>
#include <map>

#define SERVER_CONN_TIMEOUT 10

#if _WIN32
    #include "curl/curl.h"
#else
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
    #include <netdb.h>
    #include <curl/curl.h>
#endif

namespace QLicenseService {

struct HttpRequest {
    std::string payload;
    std::string url;
    std::string reply;
    curl_slist *headers = nullptr;
};

/*********************
    HttpClient class
*/

class HttpClient
{
public:
    explicit HttpClient(const std::string &serverUrl);

    ~HttpClient() {}

    int sendAndReceive(std::string &reply, const std::string &payload,
                const std::string &accessPoint, const std::string &server,
                const std::string &authKey="");
private:
    std::string m_serverUrl;
    std::string m_userAgent = "License daemon / ";
    int doRequest(CURL *curl, HttpRequest &request);
};

} // namespace QLicenseService
