/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "abstractfactory.h"
#include "clienthandler.h"

namespace QLicenseService {

class ClientHandlerFactory
    : public AbstractFactory<ClientHandler, std::string, const RequestInfo &, const LicdSetup &>
{
public:
    static ClientHandlerFactory &instance();

    template <typename T>
    void registerHandler(const std::string &name)
    {
        registerProduct<T>(name);
    }

private:
    ClientHandlerFactory();
};

} // namespace QLicenseService
