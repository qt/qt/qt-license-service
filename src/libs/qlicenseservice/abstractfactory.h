/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <unordered_map>

namespace QLicenseService {

template <typename TBase, typename TId, typename... TArgs>
class AbstractFactory
{
public:
    virtual ~AbstractFactory() {}

    using FactoryMethod = TBase *(*)(TArgs...);

    template <typename TDerived>
    void registerProduct(const TId &id)
    {
        m_products.insert(std::make_pair<const TId &, FactoryMethod>
            (id, &AbstractFactory::create<TDerived>));
    }

    void registerProduct(const TId &id, FactoryMethod func)
    {
        m_products.insert(std::make_pair<const TId &, FactoryMethod>(id, func));
    }

    bool containsProduct(const TId &id) const
    {
        auto it = m_products.find(id);
        return !m_products.cend();
    }

    TBase *create(const TId &id, TArgs... args) const
    {
        auto it = m_products.find(id);
        if (it == m_products.cend())
            return nullptr;

        return (it->second)(std::forward<TArgs>(args)...);
    }

protected:
    AbstractFactory() = default;

private:
    template <typename TDerived>
    static TBase *create(TArgs... args)
    {
        return new TDerived(std::forward<TArgs>(args)...);
    }

    AbstractFactory(const AbstractFactory &) = delete;
    AbstractFactory &operator=(const AbstractFactory &) = delete;

private:
    std::unordered_map<TId, FactoryMethod> m_products;
};

} // namespace QLicenseService
