/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <string>

namespace QLicenseService {

enum RequestReply {
    e_bad_request               = -1,
    e_got_response              = 0,
    e_license_granted           = 1,
    e_license_rejected          = 2,
    e_no_conn_leeway            = 3,
    e_license_pool_full         = 4,
    e_no_permanent_to_release   = 5,
    e_bad_connection            = 6
};

enum class RequestType {
    no_request              = 0,
    license_request         = 1,
    keepalive_report        = 2,
    license_release         = 3,
    long_term_request       = 4,
    server_version          = 5,
    daemon_version          = 6,
    reservation_query       = 7,
};

// Struct to store request info
struct RequestInfo {
    uint16_t socketId;
    RequestType reqType = RequestType::no_request;
    uint16_t    updateIntervalSecs;
    std::string licenseFile;
    std::string reservationID;
    std::string operation;
    std::string appName;
    std::string appVersion;
    std::string userId;
    std::string licenseId;
    std::string email;
    std::string payload;
    std::string serverAddr;
    std::string accessPoint;
    uint64_t    startTimestamp = 0;      // used by QA-Tools only
    uint64_t    stopTimestamp = 0;      // used by QA-Tools only
    std::string runnerType;             // "qa_tester" ||  "qa_exe", used by QA-Tools only
    std::string parentReservationId;    // used by QA-Tools only
};

} // namespace QLicenseService
