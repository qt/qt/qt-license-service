/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include <iostream>
#include <string>
#include <map>

namespace QLicenseService {

enum settings_file_result {
    e_settings_ok           = 0,
    e_general_file_error    = 1,
    e_file_not_present      = 2,
    e_no_settings_tag       = 3,
    e_no_defaults           = 4,
    e_file_invalid_content  = 5
};

enum settings_owner_type {
    e_set_type_daemon       = 0,
    e_set_type_moc          = 1,
    e_set_type_licensetool  = 2,
    e_set_type_squish       = 3,
    e_set_type_squish_ide   = 4,
    e_aet_type_coco         = 5,
    e_set_type_other
};

class LicdSetup
{
public:
    explicit LicdSetup(int type, const std::string &settingsFilePath
            , const std::string &callerBinaryPath = "");
    ~LicdSetup() {}
    std::string get(const std::string &item);
    void set(const std::string &item, const std::string &value);
    int initSettings();

    static std::string getQtAppDataLocation();

private:
    std::string m_settingsFilePath;
    std::string m_tag;
    std::map<std::string, std::string> m_settings;
    int m_setType;

    void setHwId();
    int getUserInfo();
    int getDaemonInfo();
    std::string parseSettingTag(const std::string &binPath);
    int createUserSettingsFile();
    int appendNewSection(const std::string &content);
    std::string getTaggedSettingsSkeleton();
    std::string getUserFileSkeleton();
};

} // namespace QLicenseService
