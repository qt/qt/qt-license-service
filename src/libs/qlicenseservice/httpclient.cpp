/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "httpclient.h"

#include "version.h"

namespace QLicenseService {

size_t WriteCallback(char *contents, size_t size, size_t nmemb, void *userp)
{
    //std::cout << "Reading data\n";
    ((std::string *)userp)->append(contents, size * nmemb);
    return size * nmemb;
}

HttpClient::HttpClient(const std::string &serverUrl)
    : m_serverUrl(serverUrl)
{
    m_userAgent += DAEMON_VERSION;
}

int HttpClient::sendAndReceive(std::string &reply, const std::string &payload,
                    const std::string &accessPoint, const std::string &server,
                    const std::string &authKey)
{
    if (accessPoint.empty()) {
        std::cout << "[HttpClient] error: No server endpoint given\n";
        return 1;
    }
    HttpRequest request;

    // specify URL
    if (server.empty()) {
        // If server URL is not given as param, we use the default
        request.url = m_serverUrl;
    }
    request.url += accessPoint;
    request.payload = payload;

    if (!authKey.empty()) {
        // Add auth key in headers
        std::string auth = "Authorization: " + authKey;
        request.headers = curl_slist_append(request.headers, auth.c_str());
    }

    std::string agent = "User-Agent: " + m_userAgent;
    request.headers = curl_slist_append(request.headers, agent.c_str());
    request.headers = curl_slist_append(request.headers, "Accept: */*");
    request.headers = curl_slist_append(request.headers, "Content-Type: application/json");
    request.headers = curl_slist_append(request.headers, "charset: utf-8");

    int retVal = 0;

    /* init the curl session */
    curl_global_init(CURL_GLOBAL_ALL);
    CURL *curl = curl_easy_init();
    try {
        if (doRequest(curl, request) != 0) {
            retVal = 1;
        }
    } catch(...) {
        std::cout << "Connection failed\n";
        retVal = 1;
    }
    if (retVal == 0) {
        std::cout << request.reply.length() << " bytes retrieved from license server\n";
    }

    /* cleanup curl stuff */

    curl_easy_cleanup(curl);
    curl_global_cleanup();
    curl_slist_free_all(request.headers);
    reply = request.reply;
    return retVal;
}

int HttpClient::doRequest(CURL *curl, HttpRequest &request)
{
    // Set all received data to be send to our callback function
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);

    std::string readBuffer;
    // Pass the buffer to a callback
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);

    // Set the URL / headers
    curl_easy_setopt(curl, CURLOPT_URL, request.url.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, request.headers);
    if (!request.payload.empty()) {
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, 1);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request.payload.c_str());
    }
    else {
        curl_easy_setopt(curl, CURLOPT_HTTPGET, 1); // for server ping
    }
    if (curl_easy_setopt(curl, CURLOPT_USE_SSL, (long)CURLUSESSL_ALL) != CURLE_OK) {
        std::cout << "Warning! No SSL support available\n";
    }
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, SERVER_CONN_TIMEOUT);

    // get it
    CURLcode res;
    res = curl_easy_perform(curl);

    // check for errors
    if (res != CURLE_OK) {
        std::cout << "HTTP transfer failed, URL: " << request.url << std::endl;
        std::cout << curl_easy_strerror(res) << std::endl;
        return 1;
    }
    request.reply = readBuffer;
    return 0;
}

} // namespace QLicenseService
