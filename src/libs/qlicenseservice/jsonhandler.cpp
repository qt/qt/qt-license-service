/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "jsonhandler.h"

#include "utils.h"

namespace QLicenseService {

JsonHandler::JsonHandler(const std::string &jsonString)
{
    if (preParse(jsonString) != 0) {
        std::cout << "Error reading JSON:\n" << jsonString << std::endl;
    }
}

std::string JsonHandler::get(const std::string &item)
{
    return getStr(item);
}

int JsonHandler::getInt(const std::string &item)
{
    if (!m_items.count(item)) {
        std::cout << "ERROR no item '" << item << "' (integer) found in JSON\n";
        return INT16_MIN;
    }
    // Is it an integer?
    try {
        return std::stoi(m_items[item]);
    }
    catch (...) {
        return INT16_MIN;
    }

}

std::string JsonHandler::getStr(const std::string &item)
{
    if (!m_items.count(item)) {
        std::cout << "ERROR no item '" << item << "' (string) found in JSON\n";
        return("Error in license service");
    }
    return (std::string)m_items[item];
}

bool JsonHandler::getBool(const std::string &item)
{
    if (!m_items.count(item)) {
        std::cout << "ERROR no item '" << item << "' (boolean) found in JSON\n";
    }
    return (m_items[item] == "true")? true : false;
}

void JsonHandler::add(const std::string &item, const std::string &value)
{
    m_items[item] = value;
}

void JsonHandler::add(const std::string &item, uint64_t value)
{
    m_items[item] = std::to_string(value);
}

void JsonHandler::add(const std::string &item, bool value)
{
    m_items[item] = (value) ? "true" : "false";
}

std::string JsonHandler::dump(uint8_t indent)
{
    // Stringify a JSON
    // NOTE!! Specific to license daemon, NOT GENERIC
    std::string ind(indent, ' ');
    uint8_t itemCount = m_items.size();
    std::stringstream top; // For JSON top level
    std::stringstream res; // for items going under "reservation"
    top << "{";
    if (indent > 0) {
        res << ind <<  "\"reservation\": {" << std::endl;
    } else {
        res << "\"reservation\":{";
    }
    top << std::endl;

    uint8_t count = 0;
    for (auto const &item : m_items) {
        count ++;
        if (std::find(reservation.begin(), reservation.end(),
                        item.first) != reservation.end()) {
            res << ind << ind <<  "\"" << item.first << "\":";
            if (indent > 0) res << " ";
            res << "\"" << item.second << "\"";
            if (count == itemCount) {
                if (indent > 0) {
                    res << std::endl << ind << "}";
                } else {
                    res << "}" << std::endl;
                }
                continue;
            } else res << ",";
            if (indent > 0) res << std::endl;
        } else {
            if (indent > 0) res << std::setfill(' ') << std::setw(indent);
            top << ind << "\"" << item.first << "\":";
            if (indent > 0) top << " ";
            top << "\"" << item.second << "\",";
            if (indent > 0) top << std::endl;
        }
    }
    top << res.str();
    if (indent > 0) top << std::endl;
    top << "}";
    return top.str();
}

int JsonHandler::preParse(const std::string &jsonString)
{
    std::string str = jsonString;
    /* First throw away everything we dont need
    */
    size_t idx = str.find("\"reservation\":");
    // To prevent a crash if that string is not found:
    if (idx != std::string::npos )
        str.erase(idx +1, 13);
    str.erase(std::remove(str.begin(), str.end(), '{'), str.end());
    str.erase(std::remove(str.begin(), str.end(), '}'), str.end());
    str.erase(std::remove(str.begin(), str.end(), '"'), str.end());

    // Split the item/value pairs
    std::vector<std::string> items = utils::splitStr(str, ',');

    // Populate m_items map
    for (const std::string &pair : items) {
        std::string item = pair.substr(0, pair.find(':'));
        item = utils::trimStr(item);
        std::string value = utils::trimStr(pair.substr(pair.find(':') + 1, pair.length() - 1));
        m_items[item] = value;
    }
    return 0;

}

} // namespace QLicenseService
