/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "commonsetup.h"

#include "qtlicensetool.h"

using namespace QLicenseService;

int main(int argc, char *argv[])
{
    // Fetch the settings from .ini file
    LicdSetup setup(e_set_type_licensetool, LicdSetup::getQtAppDataLocation() + USER_SETTINGS_FILE);
    if (setup.initSettings() == e_file_not_present) {
        errorAndExit("Not able to find the settings");
    }
    if (argc < 2) {
        errorAndExit("No arguments given");
    }
    std::string action = argv[1];
    // Get rid of commands which don't require further actions
    if (action == "--version" || action == "-v") {
        showVersion();
        return 0;
    } else if (action == "--help" || action == "-h") {
        helpAndExit();
    }

    // Then those requiring connection
    if (action == "--permanent" || action == "-p") {
        // Override setup with cmd-line args:
        overrideSetup(&setup, argc, argv);
        doPermanentRequest(&setup);
    } else if (action == "--serverversion" || action == "-S") {
        askStatus(SERVER_VERSION_CMD, &setup);
    } else if (action == "--daemonversion" || action == "-D") {
        askStatus(DAEMON_VERSION_CMD, &setup);
    } else if (action == "--reservation" || action == "-r") {
        askStatus(RESERVATION_QUERY_CMD, &setup);
    } else {
        errorAndExit("Invalid parameters");
    }
}
