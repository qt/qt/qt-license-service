# Copyright (C) 2023 The Qt Company Ltd.
#
# Published under GPL-3.0 with Qt-GPL-exception-1.0
#

add_executable(tst_licenseservice tst_licenseservice.cpp)
target_link_libraries(tst_licenseservice PRIVATE Catch2 qlicenseservice)

add_test(NAME tst_licenseservice COMMAND tst_licenseservice)
