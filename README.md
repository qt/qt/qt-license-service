// Copyright (C) 2022 The Qt Company Ltd.
//

Qt Licenser daemon for Windows and Linux

Daemon is a gateway between Qt License Server and Qt developer tools (i.e. Qt Creator and Qt Design Studio).
It also keeps users developing Qt apps without Qt tools under valid license by wrapping the MOC (Meta Object Compiler)
into new one which requests license like other tools so.

Daemon talks to the server via HTTP, with data moving back and worth in JSON format using HTTP POST method. Clients request for their licenses by connectiong to daemons's TCP/IP server, using plain text.

For installation/running, see each system's respective INSTALL_xxx.txt files.

#  The user settings file

The settings file 'qtlicenseservice.ini' is created automatically in user's Qt home folder which usually is in:
Linux:   <your home dir>/.local/share/Qt/qtlicenseservice.ini
Windows: <your home dir>\AppData\Roaming\Qt\qtlicenseservice.ini

If the user's Qt directory is not in place, please create it with:
Linux:
  > mkdir <your home dir>/.local/share/Qt
Windows:
  > mkdir <your home dir>\AppData\Roaming\Qt

To create it from scratch, run either mocwrapper or qtlicensetool once (qtlicensetool needs some parameter - you can use -S). You'll be informed about the creation and location of the settings file. The defaults most probably are not correct, so edit it to meet your configuration. The most critical settings are:
   - license_id  Your license id, you definitely must edit this
   - licd_addr   Address to the machine where License Service (daemon) is running. In many cases, localhost is correct.
   - licd_port   Daemon port - default is 60000. Check from daemon settings file or ask the admin

  -- MOC-wrapper/moc

From now on, moc will take the [default] section in use if there's no installation-specific section available. New section names will be determined by current Qt framework installation path. Thats why its mandatory to call moc with absolute path on the second run - easisest way to do that is to just start building some Qt app, as the build procedure always calls moc with absolute path.
You'll be informed about the creation of the new section under which the defaults are copied at this point. In case you have multiple instances/versions of the Qt framework, you'll have invividual sections for each of them.

  -- qtlicensetool
qtlicensetool ALWAYS reads its settings from the [default] section, no matter where it's being run from.


# Setup and Usage

At first, start Qt License Server on some Windows machine

- If you didn't edit daemon settings after building: Check the ip address from Win box where the License Server is running, then copy it  to the line stating 'server_addr' in the settings file (qtlicd.ini). This will be user as the default site license server address. After installation, it must be edited:
    Linux: (as root) in /opt/qtlicd/qtlicd.ini
    Widows (as admin) in C:\Program Files/qtlicd/qtlicd.ini
Start the daemon, as described in respective Linux/Windows INSTALL file.

- You also can 'manually' start qtlicd in no-daemon mode (as normal CLI app):
    - First make sure daemon is not running
    - from the command line, cd to the directory where daemon is installed, then give a command:
        > ./qtlicd --nodaemon (Linux/Mac)
        > qtlicd.exe --nodaemon (Windows)
    - You now can see immediately what happens in there. Stop like any other app with Ctrl-C
    - Note that where ever you start qtlicd, it will always read settings from installation folder.

--  MOC-wrapper

    If the setup is made correct (moc binary changed to the link pointing to mocwrapper), MOC license checks should happen in backround without any user actions, each time a build is being done.

    You can ask mocwrapper version with:
    > ./mocwrapper --version (mocwrapper.exe in Windows)

--  qtlicensetool

    Qtlicensetool is a CLI command to handle more Qt licensing-related checks and actions. Command has a format:
        qtlicensetool <Action> [params]
    Where 'Action' is:
        -h or --help                : Prints out the tool's help text
        -v or --version             : Prints out qtlicensetool version
        -S or --serverversion       : Prints out License Server version
        -D or --daemonversion       : Prints out the License Service (daemon) version
        -L or --longterm            : Longterm reservation (add or remove)
    For longterm usage ('-L' or '--longterm'), you must tell which operation to do:
        add|remove  <--- Either one needs to be there
    Optional parameters which override [default] section settings:
        -d = Full license daemon address and port separated by ':'
        -u = Your Qt license username
        -i = Your Qt license ID number (must be an int)
    Example:
        > qtlicensetool --longterm add          : Make a long-term license reservation
        > qtlicensetool -L add -i 462412        : Same as above, but using different license ID
        > qtlicensetool --daemonversion         : Prints out a daemon version number

--  To  test MOC and plugin interfaces without actually using MOC or Qt Creator (plugin):

    Use any TCP/IP-enabled app to connect to localhost's port 60000 (or whichever is stated in qtlicd.ini). For example, with telnet:
        telnet localhost 60000

    After connecting, issue a command like:
        license -u foobar -a QtCreator -v 6.3 -i 123456
            Where: -u = username, -a = app in use, -v app version and -i Qt license ID

    Reply is then info txt about having/not having a valid license, or "Bad request" for malformed message.

#  Version info  ----------

From each component included, you can ask their version using --version switch as follows:
    [app name] --version   <--- [app name].exe in Windows, of course
        - where [app name] can be one of: 'qtlicd', 'qtlicensetool', 'moc' (or 'mocwrapper')
        - Note! By default, 'qtlicd' and 'mocwrapper' are not in $PATH by default, so you must call them with their absolute path.
           In Windows case, you can add "C:\Program Files\qtlicd\" in environment PATH to ease up calling 'qtlicensetool'
In Linux, the binaries are in /opt/qtlicd after installation, and "C:\Program Files\qtlicd\" in Windows.
