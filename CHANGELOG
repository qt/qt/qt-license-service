Qt License daemon changelog

Changes from 0.xxx to 2.0.0

- 'qtlongterm' CLI tool replaced qith new, more generic 'qtlicensetool'
- Long-term functionality added in qtlicensetool through -L (or --longterm) switch
- qtlicensetool always uses [default] section in user settings file to get its settings from
    * Swithces used in qtlongterm command are now mostly optional, to be able to override the [default] ones
    * See 'qtlicensetool --help' or README.md
- Version queries now possible with qtlicensetool
    * Version queries from License Service (daemon), License server and the tool itself
- The binaries (licd, mocwrapper, qtlicensetool) now accept --version (or -v) switch
- Several under-the-hood improvements for maintainability and robustness

####################

Changes in 2.0.1:
- Added a new feature to qtlicensetool: Get current reservation status
- Mocwrapper is now less verbose

####################

Changes in 2.0.2:
- Added this changelog
- Implemented support for additional leeway time in case there's no server connection

####################

Changes in 2.0.3:
- Mocwrapper made even less verbose

####################

Changes in 2.0.4:
- Changed daemon response message in case there is no licenses left in the pool

Changes in 2.1.0:
- Preliminary floating license support added for Squish
- Daemon name 'licd' changed to 'qtlicd'
- Daemon architecture changed towards more modular approach
- Fixed a bug with SSL support
- Added HW ID calculation if empty (hw_id now empty by default in qtlicd.ini)
- Removed possibility to override license server address from CLI
- Concept of "long-term" renamed to "permanent"
        '-L' switch of qtlicensetool changed to '-p' to reflect the change
- Fixed cloud server support
