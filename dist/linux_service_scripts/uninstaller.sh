#!/usr/bin/env bash
# Copyright (C) 2022 The Qt Company Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0

name=qtlicd

function stop_daemon {
    running=true

    retries=0
    while [ $running != "false" ]
    do
        running=false
        systemctl is-active --quiet $name && running=true;
        if [ $running != "false" ]
        then
            systemctl stop $name
            sleep 2
            retries=$((retries+1))
            if [ $retries -gt 2 ]
            then
                exit_fail "Tried to stop qtlicenser service for $retries times already"
            fi
        fi
    done
    echo "Stopped $name.service"
}

stop_daemon

systemctl disable ${name}
rm -rf /opt/${name}
rm /usr/bin/qtlicensetool
rm /etc/systemd/system/${name}.service
systemctl daemon-reload
